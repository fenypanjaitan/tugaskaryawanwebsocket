package org.example;

import org.example.connection.DbConn;
import org.json.JSONObject;

import java.sql.*;


public class Karyawan {
    Connection connection = new DbConn().runConnection();
    static Statement statement;
    static ResultSet resultSet;

    void addData(String theJSON) throws SQLException {
        JSONObject theResponse = new JSONObject(theJSON);

        String nik = theResponse.getString("nik");
        String nama = theResponse.getString("nama");
        String tempatlahir = theResponse.getString("tempatlahir");
        String tanggallahir = theResponse.getString("tanggallahir");
        String gender = theResponse.getString("gender");
        String alamat = theResponse.getString("alamat");
        String rtrw = theResponse.getString("rtrw");
        String keldes = theResponse.getString("keldes");
        String kecamatan = theResponse.getString("kecamatan");
        String agama = theResponse.getString("agama");
        String status = theResponse.getString("status");
        String pekerjaan = theResponse.getString("pekerjaan");
        String kwn = theResponse.getString("kwn");

        queryIn(nik, nama,tempatlahir,tanggallahir, gender,alamat,rtrw, keldes, kecamatan,agama,status,pekerjaan,kwn);
    }

    void queryIn(String nik, String nama, String tempatlahir, String tanggallahir, String gender, String alamat, String rtrw, String keldes, String kecamatan, String agama, String status,String pekerjaan, String kwn) throws SQLException {
        String query = "INSERT INTO data VALUES ('"+nik+"', '"+nama+"','"+tempatlahir+"','"+tanggallahir+"','"+gender+"','"+alamat+"','"+rtrw+"','"+keldes+"','"+kecamatan+"','"+agama+"','"+status+"','"+pekerjaan+"','"+kwn+"')";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.executeUpdate(query);
    }


}
