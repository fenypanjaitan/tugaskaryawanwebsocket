package org.example.connection;

import org.json.JSONObject;

import java.sql.*;

public class Karyawan {
    public static void main(String[] args) {
        Karyawan karyawan = new Karyawan();
        karyawan.dataKaryawan();
    }
    public void dataKaryawan(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/karyawan?useUnicode=true&useJDBCCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root","");

            Statement stmt = myConn.createStatement();
            ResultSet res = stmt.executeQuery("SELECT * from data");
            while (res.next()){
                JSONObject myObj = new JSONObject();
                myObj.put("NIK", res.getString(2));
                myObj.put("Nama", res.getString(3));
                myObj.put("Alamat", res.getString(4));
                System.out.println(myObj.toString());

            }
            myConn.close();
        } catch (SQLException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }
}
